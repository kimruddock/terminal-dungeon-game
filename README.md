### The Terminal Dungeon Game ###

A quick, fun text-based game intended to help people get used to navigating folder structures in the command line.

### Getting Started ###

* Unzip the repository folder and put it somewhere easy to find like the desktop
* Open up Terminal (either in Applications -> Utilities or press cmd + space and search for Terminal)
* Navigate to the repository folder using 'cd' command (change directory) or just type 'cd ' and drag the folder from your desktop on to the terminal screen
* Type 'ls' and make sure that Welcome.txt is in the current folder
* Type 'nano Welcome.txt' to start the game!

### Game Controls ###

* cd [directory name] - open door (change directory)
* cd ../ - go back a room (move up a directory)
* ls - look around room (list directory contents)
* nano [file name] - read room text file (open nano terminal program)