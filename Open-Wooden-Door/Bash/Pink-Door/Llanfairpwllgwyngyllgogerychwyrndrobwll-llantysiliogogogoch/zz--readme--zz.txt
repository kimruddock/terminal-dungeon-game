-----------------------------------------
Llanfairpwllgwyn...whatever
-----------------------------------------

Bash: "A thousand curses upon you! How did you get through that door so 
quickly?! No matter, you have now entered the final part of your quest and now
 face your toughest challenge... the Labyrinth of Doom and Confusion and 
Misery and Sadness and Insanity!"

You look around. To be fair this looks tough. Ahead of you is a maze of 
hundreds of doors each with a cryptic code etched into the surface.

Bash: "Choose the door with the correct code and you will leave the Terminal 
Dungeon. Choose incorrectly and a wet, fishy treat awaits! MAHAHAAHAHAHHAHAA!!" 

Bash runs off laughing manically to himself. What a bastard.

You sit on the floor. It was all going so well, surely you can't be expected 
to try every door? You'd be here for... eternity. Just as you begin to give up
 all hope your fez-endowed monkey friend reappears with another note:

"You can find the code scratched into a hidden panel on the wall to your left.
 To find the hidden panel type ls -a at the Terminal prompt. The command ls -a
 will display all files in a folder including hidden files such as .htaccess.
 From, A Friend"

-----------------------------------------

ctrl-x to exit nano and type ls -a (you might need to scroll up to the top of
 the listed contents as there are so many folders in here)
